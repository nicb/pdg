# Giuseppe *Peppino* Di Giugno - Short paper - August 2020

Short paper written in honor of Giuseppe Di Giugno in August 2020 (original version in Italian).

Piccolo scritto redatto in onore di Giuseppe Di Giugno nell'agosto 2020 (versione originale in italiano).

# LICENSE

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">PDG</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://gitlab.com/nicb/PDG" rel="dct:source">https://gitlab.com/nicb/PDG"</a>.
[Full text license](./LICENSE)
